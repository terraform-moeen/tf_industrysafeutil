terraform {
    backend "remote" {
        hostname     = "app.terraform.io"
        organization = "Northbay"
        workspaces {
            name = "tf-tutorial"
        }
    }
}
