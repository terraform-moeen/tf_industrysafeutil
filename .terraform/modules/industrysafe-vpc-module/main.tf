# This template creates a VPC with subnets, route tables, internet gateway, nat gateway, vpc endpoint and dhcp option

# Resources
# Create VPC
resource "aws_vpc" "industrysafe-vpc" {
  cidr_block           = var.VpcCidrBlock
  enable_dns_support   = var.DnsSupport
  enable_dns_hostnames = var.DnsHostNames
  tags = {
    Name = var.VpcName
  }
} # end resource

# Create Public Subnet 1 
resource "aws_subnet" "industrysafe-public-subnet-1" {
  vpc_id                  = aws_vpc.industrysafe-vpc.id
  cidr_block              = var.PublicSubnet1CidrBlock
  map_public_ip_on_launch = var.MapPublicIP
  availability_zone       = var.AvailabilityZone1
  tags = {
   Name = var.PublicSubnet1Name
  }
} # end resource

# Create Public Subnet 2
resource "aws_subnet" "industrysafe-public-subnet-2" {
  vpc_id                  = aws_vpc.industrysafe-vpc.id
  cidr_block              = var.PublicSubnet2CidrBlock
  map_public_ip_on_launch = var.MapPublicIP
  availability_zone       = var.AvailabilityZone2
  tags = {
   Name = var.PublicSubnet2Name
  }
} # end resource

# Create Private Subnet 1
resource "aws_subnet" "industrysafe-private-subnet-1" {
  vpc_id            = aws_vpc.industrysafe-vpc.id
  cidr_block        = var.PrivateSubnet1CidrBlock
  availability_zone = var.AvailabilityZone1
  tags = {
   Name = var.PrivateSubnet1Name
  }
} # end resource

# Create Private Subnet 2
resource "aws_subnet" "industrysafe-private-subnet-2" {
  vpc_id            = aws_vpc.industrysafe-vpc.id
  cidr_block        = var.PrivateSubnet2CidrBlock
  availability_zone = var.AvailabilityZone2
  tags = {
   Name = var.PrivateSubnet2Name
  }
} # end resource

#Create Secret Subnet 1
resource "aws_subnet" "industrysafe-secret-subnet-1" {
  vpc_id            = aws_vpc.industrysafe-vpc.id
  cidr_block        = var.SecretSubnet1CidrBlock
  availability_zone = var.AvailabilityZone1
  tags = {
   Name = var.SecretSubnet1Name
  }
} # end resource

# Create Secret Subnet 2
resource "aws_subnet" "industrysafe-secret-subnet-2" {
  vpc_id            = aws_vpc.industrysafe-vpc.id
  cidr_block        = var.SecretSubnet2CidrBlock
  availability_zone = var.AvailabilityZone2
  tags = {
   Name = var.SecretSubnet2Name
  }
} # end resource

# Create Internet Gateway
resource "aws_internet_gateway" "industrysafe-internet-gateway" {
  vpc_id  = aws_vpc.industrysafe-vpc.id
  tags = {
    Name = var.InternetGatewayName
  }
} # end resource

# Create Public Route Table for VPC
resource "aws_route_table" "industrysafe-public-route-table" {
  vpc_id = aws_vpc.industrysafe-vpc.id
  tags = {
    Name = var.PublicRouteTableName
  }
} # end resource

# Create Public Route in the public route table created above
resource "aws_route" "industrysafe-public-route" {
  route_table_id         = aws_route_table.industrysafe-public-route-table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.industrysafe-internet-gateway.id
} # end resource

# Associate public subnet 1 with public route table
resource "aws_route_table_association" "industrysafe-public-subnet-1-route-table-association" {
  subnet_id      = aws_subnet.industrysafe-public-subnet-1.id
  route_table_id = aws_route_table.industrysafe-public-route-table.id
} # end resource

# Associate public subnet 2 with public route table
resource "aws_route_table_association" "industrysafe-public-subnet-2-route-table-association" {
  subnet_id      = aws_subnet.industrysafe-public-subnet-2.id
  route_table_id = aws_route_table.industrysafe-public-route-table.id
} # end resource

# Create EIP for a NAT gateway
resource "aws_eip" "industrysafe-eip-1" {
  tags = {
    Name = var.Eip1Name
  }
} # end resource

# Create NAT Gateway in public subnet 1
resource "aws_nat_gateway" "industrysafe-ngw-1" {
  allocation_id = aws_eip.industrysafe-eip-1.id
  subnet_id     = aws_subnet.industrysafe-public-subnet-1.id
  tags = {
    Name = var.NatGateway1Name
  }
} # end resource

# Create Private Route Table 
resource "aws_route_table" "industrysafe-private-route-table-1" {
  vpc_id = aws_vpc.industrysafe-vpc.id
  tags = {
    Name = var.PrivateRouteTable1Name
  }
} # end resource

# Create Private Route in Private Route Table
resource "aws_route" "industrysafe-private-route-1" {
  route_table_id         = aws_route_table.industrysafe-private-route-table-1.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.industrysafe-ngw-1.id
} # end resource

# Associate private subnet 1 with private route table
resource "aws_route_table_association" "industrysafe-private-subnet-1-route-table-1-association" {
  subnet_id      = aws_subnet.industrysafe-private-subnet-1.id
  route_table_id = aws_route_table.industrysafe-private-route-table-1.id
} # end resource

# Create EIP for second NAT gateway
resource "aws_eip" "industrysafe-eip-2" {
  tags = {
    Name = var.Eip2Name
  }
} # end resource

# Create NAT Gateway in public subnet 2
resource "aws_nat_gateway" "industrysafe-ngw-2" {
  allocation_id = aws_eip.industrysafe-eip-2.id
  subnet_id     = aws_subnet.industrysafe-public-subnet-2.id
  tags = {
    Name = var.NatGateway2Name
  }
} # end resource

# Create second Private Route Table 
resource "aws_route_table" "industrysafe-private-route-table-2" {
  vpc_id = aws_vpc.industrysafe-vpc.id
  tags = {
    Name = var.PrivateRouteTable2Name
  }
} # end resource

# Create Private Route for second Private Route Table 
resource "aws_route" "industrysafe-private-route-2" {
  route_table_id         = aws_route_table.industrysafe-private-route-table-2.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.industrysafe-ngw-2.id
} # end resource

# Associate private subnet 2 with second private route table
resource "aws_route_table_association" "industrysafe-private-subnet-2-route-table-2-association" {
  subnet_id      = aws_subnet.industrysafe-private-subnet-2.id
  route_table_id = aws_route_table.industrysafe-private-route-table-2.id
} # end resource

# Create VPC endpoint for S3
resource "aws_vpc_endpoint" "industrysafe-vpc-endpoint" {
  vpc_id       = aws_vpc.industrysafe-vpc.id
  service_name = var.VpcEndpointServiceName
  tags = {
    Name = var.VpcEndpointName
  }
} # end resource

# Create VPC endpoint Route Table Association
resource "aws_vpc_endpoint_route_table_association" "industrysafe-vpc-endpoint-private-route-table-1" {
  route_table_id  = aws_route_table.industrysafe-private-route-table-1.id
  vpc_endpoint_id = aws_vpc_endpoint.industrysafe-vpc-endpoint.id
} # end resource

# Create VPC endpoint Route Table Association
resource "aws_vpc_endpoint_route_table_association" "industrysafe-vpc-endpoint-private-route-table-2" {
  route_table_id  = aws_route_table.industrysafe-private-route-table-2.id
  vpc_endpoint_id = aws_vpc_endpoint.industrysafe-vpc-endpoint.id
} # end resource

# Create VPC DHCP Options
resource "aws_vpc_dhcp_options" "industrysafe-vpc-dhcp-options" {
  count               = var.CreateDhcpOptions==true ? 1 : 0
  domain_name         = var.DhcpDomainName
  domain_name_servers = var.DhcpDomainNameServers
  ntp_servers         = var.DhcpNtpServers
  tags = {
    Name = var.DhcpOptionsName
  }
} # end resource

# Create DHCP Options association with the VPC
resource "aws_vpc_dhcp_options_association" "industrysafe-vpc-dhcp-options-association" {
  count           = var.CreateDhcpOptions==true ? 1 : 0
  vpc_id          = aws_vpc.industrysafe-vpc.id
  dhcp_options_id = aws_vpc_dhcp_options.industrysafe-vpc-dhcp-options[count.index].id
} # end resource
