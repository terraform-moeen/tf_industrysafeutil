# Outputs

output "VpcId" {
  value       = aws_vpc.industrysafe-vpc.id
  description = "ID of the VPC."
}
output "PublicSubnet1Id" {
  value       = aws_subnet.industrysafe-public-subnet-1.id
  description = "ID of the public subnet in AZ 1."
}
output "PublicSubnet2Id" {
  value       = aws_subnet.industrysafe-public-subnet-2.id
  description = "ID of the public subnet in AZ 2."
}
output "PrivateSubnet1Id" {
  value       = aws_subnet.industrysafe-private-subnet-1.id
  description = "ID of the private subnet in AZ 1."
}
output "PrivateSubnet2Id" {
  value       = aws_subnet.industrysafe-private-subnet-2.id
  description = "ID of the private subnet in AZ 2."
}
output "SecretSubnet1Id" {
  value       = aws_subnet.industrysafe-secret-subnet-1.id
  description = "ID of the secret subnet in AZ 1."
}
output "SecretSubnet2Id" {
  value       = aws_subnet.industrysafe-secret-subnet-2.id
  description = "ID of the secret subnet in AZ 2."
}
