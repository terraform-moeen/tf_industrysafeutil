# Inputs

# Parameters within this module

variable "DnsSupport" {
  description = "A boolean flag to enable/disable DNS support in the VPC."
}
variable "DnsHostNames" {
  description = "A boolean flag to enable/disable DNS hostnames in the VPC."
}
variable "AvailabilityZone1" {
  description = "The AZ for the subnets."
}
variable "AvailabilityZone2" {
  description = "The AZ for the subnets."
}
variable "VpcCidrBlock" {
  description = "The CIDR block of the VPC."
}
variable "PublicSubnet1CidrBlock" {
  description = "The CIDR block for the public subnet in AZ 1."
}
variable "PublicSubnet2CidrBlock" {
  description = "The CIDR block for the public subnet in AZ 2."
}
variable "PrivateSubnet1CidrBlock" {
  description = "The CIDR block for the private subnet in AZ 1."
}
variable "PrivateSubnet2CidrBlock" {
  description = "The CIDR block for the private subnet in AZ 2."
}
variable "SecretSubnet1CidrBlock" {
  description = "The CIDR block for the secret subnet in AZ 1."
}
variable "SecretSubnet2CidrBlock" {
  description = "The CIDR block for the secret subnet in AZ 2."
}
variable "MapPublicIP" {
  description = "Specify true to indicate that instances launched into the subnet should be assigned a public IP address."
}
variable "VpcEndpointServiceName" {
  description = "The service name, in the form 'com.amazonaws.region.service' for AWS services."
}
variable "VpcName" {
  description = "Name for the VPC."
}
variable "PublicSubnet1Name" {
  description = "Name for public subnet 1."
}
variable "PublicSubnet2Name" {
  description = "Name for public subnet 2."
}
variable "PrivateSubnet1Name" {
  description = "Name for private subnet 1."
}
variable "PrivateSubnet2Name" {
  description = "Name for private subnet 2."
}
variable "SecretSubnet1Name" {
  description = "Name for secret subnet 1."
}
variable "SecretSubnet2Name" {
  description = "Name for secret subnet 2."
}
variable "InternetGatewayName" {
  description = "Name for internet gateway."
}
variable "PublicRouteTableName" {
  description = "Name for route table associated with public subnet in both AZs."
}
variable "PrivateRouteTable1Name" {
  description = "Name for route table associated with private subnet in AZ 1."
}
variable "PrivateRouteTable2Name" {
  description = "Name for route table associated with private subnet in AZ 2."
}
variable "Eip1Name" {
  description = "Name for EIP to attach with NAT gateway in AZ 1."
}
variable "Eip2Name" {
  description = "Name for EIP to attach with NAT gateway in AZ 2."
}
variable "NatGateway1Name" {
  description = "Name for NAT gateway in AZ 1."
}
variable "NatGateway2Name" {
  description = "Name for NAT gateway in AZ 2."
}
variable "VpcEndpointName" {
  description = "Name for VPC endpoint for S3."
}
variable "DhcpDomainName" {
  description = "Domain name for DHCP options."
}
variable "DhcpDomainNameServers" {
  description = "Domain name servers for DHCP options."
}
variable "DhcpNtpServers" {
  description = "NTP servers for DHCP options."
}
variable "DhcpOptionsName" {
  description = "Name for DHCP options."
}
variable "CreateDhcpOptions" {
  description = "Specify a boolean value whether to create DHCP options or not."
}
